package mx.ita.saic4_000.maison;

/**
 * Created by hp on 03/11/2016.
 */

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class Tab1Habitacion extends Fragment{


    View rootView;
    Gallery simpleGallery;
    GalleryAdapter GalleryAdapter;
    SmartImageView imagen1;
    ArrayList Imagen = new ArrayList();
    String url;
    String[] im;
    Rect[] re;

    public Tab1Habitacion() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tab1habitacion, null);
        imagen1 = (SmartImageView)rootView.findViewById(R.id.selec);
        simpleGallery = (Gallery)rootView.findViewById(R.id.gallery);

        if(ListaVenta.id!=null) {
            url="http://s657539154.onlinehome.mx/mostrarimagenes.php?id=" + ListaVenta.id;
        }
        if(ListaRenta.id!=null) {
            url="http://s657539154.onlinehome.mx/mostrarimagenes.php?id=" + ListaRenta.id;
        }
        descargarimagen();

        // set the adapter
        // perform setOnItemClickListener event on the Gallery
        simpleGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // set the selected image in the ImageView
                String img = "http://s657539154.onlinehome.mx/imagenes/" + Imagen.get(position).toString();
                Rect rect = new Rect(imagen1.getLeft(), imagen1.getTop(),imagen1.getBottom(),imagen1.getRight());
                imagen1.setImageUrl(img,rect);
                simpleGallery.setUnselectedAlpha(0.40f);
            }
        });
        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void descargarimagen() {
        Imagen.clear();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        im=new String[jsonArray.length()];
                        re=new Rect[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Imagen.add(jsonArray.getJSONObject(i).getString("imagen"));
                            String img = "http://s657539154.onlinehome.mx/imagenes/" + Imagen.get(i).toString();
                            Rect rect = new Rect(imagen1.getLeft(), imagen1.getTop(), imagen1.getBottom(), imagen1.getRight());

                            im[i]=img;
                            re[i]=rect;

                        }
                        imagen1.setImageUrl(im[0],re[0]);
                        GalleryAdapter = new GalleryAdapter(getContext(), im, re);
                        simpleGallery.setAdapter(GalleryAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
}
