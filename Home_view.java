package mx.ita.saic4_000.maison;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
//import android.R;


public class Home_view extends AppCompatActivity {



    private TabLayout tabLayout;

    private int[] tabIcons = {
            R.drawable.ic_home_black_36dp,
            R.drawable.ic_assignment_black_36dp,
            R.drawable.ic_store_black_36dp


    };
    String id;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_view);





        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        ViewPagerAdapter1 ViewPagerAdapter1 = new ViewPagerAdapter1(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(ViewPagerAdapter1);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);


    }

        public String casa(){
            return id;
        }
    public static int s=0;

    public void escuela(View v){
        Intent lugar = new Intent(Home_view.this,Lugares.class);
        startActivity(lugar);
        s=1;
    }

    public void Hospital(View v){
        Intent lugar = new Intent(Home_view.this,Lugares.class);
        startActivity(lugar);
        s=2;
    }

    public void Tiendas(View v){
        Intent lugar = new Intent(Home_view.this,Lugares.class);
        startActivity(lugar);
        s=3;
    }

    public void Empresas(View v){
        Intent lugar = new Intent(Home_view.this,Lugares.class);
        startActivity(lugar);
        s=4;
    }

    public void Parques(View v){
        Intent lugar = new Intent(Home_view.this,Lugares.class);
        startActivity(lugar);
        s=5;
    }


}
