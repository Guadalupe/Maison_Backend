package mx.ita.saic4_000.maison;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Inicio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);


    }
    public void venta (View v){
        MediaPlayer s = MediaPlayer.create(this, R.raw.sonido_press);
        s.start();
        Filtro.preciomen="";
        Filtro.preciomay="";
        Filtro.dimmen="";
        Filtro.dimmay="";
        Filtro.no_hab="";
        Filtro.no_ban="";
        Intent Venta = new Intent(Inicio.this, ListaVenta.class);
        startActivity(Venta);
    }
    public void renta (View v){
        MediaPlayer s = MediaPlayer.create(this, R.raw.sonido_press);
        s.start();
        FiltroRenta.preciomen="";
        FiltroRenta.preciomay="";
        FiltroRenta.dimmen="";
        FiltroRenta.dimmay="";
        FiltroRenta.no_hab="";
        FiltroRenta.no_ban="";
        Intent Renta = new Intent(Inicio.this, ListaRenta.class);
        startActivity(Renta);
    }



}
