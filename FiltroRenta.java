package mx.ita.saic4_000.maison;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.ArrayList;

/**
 * Created by saic4_000 on 28/04/2017.
 */
public class FiltroRenta extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    Spinner smenor,smayor,smetrosmen,smetrosmay;
    ArrayAdapter<String> amenor,amayor,ametrosmen,ametrosmay;
    String[] conmen = new String[]{"Todos","$500.00","$600.00","$700.00","$800.00"
            ,"$900.00","$1,000.00","$1,500.00","$2,000.00","$2,500.00"};
    String[] rangprecio = new String[]{"","500","600","700","800"
            ,"900","1000","1500","2000","2500"};
    String[] conmetrosmen = new String[]{"Todos","20 m\u00B2","25 m²","30 m²","35 m²"
            ,"40 m²","50 m²","55 m²","60 m²","65 m²"};
    String[] rangodim = new String[]{"","20","25","30","35"
            ,"40","50","55","60","65"};
    ArrayList<String> conmay = new ArrayList();
    ArrayList<String> conmetrosmay = new ArrayList();
    RadioButton todoh,unoh,dosh,tresh,cuatroh,todob,unob,dosb,tresb,cuatrob;

    public static String preciomen="", preciomay="", dimmen="", dimmay="",no_hab="",no_ban="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filtro);
        smenor=(Spinner)findViewById(R.id.spinnermenor);
        smayor=(Spinner)findViewById(R.id.spinnermayor);
        smetrosmen=(Spinner)findViewById(R.id.metrosmen);
        smetrosmay=(Spinner)findViewById(R.id.metrosmay);
        todoh = (RadioButton)findViewById(R.id.todohab);
        unoh = (RadioButton)findViewById(R.id.unohab);
        dosh = (RadioButton)findViewById(R.id.doshab);
        tresh = (RadioButton)findViewById(R.id.treshab);
        cuatroh = (RadioButton)findViewById(R.id.cuatrohab);
        todob = (RadioButton)findViewById(R.id.todoba);
        unob = (RadioButton)findViewById(R.id.unoba);
        dosb = (RadioButton)findViewById(R.id.dosba);
        tresb = (RadioButton)findViewById(R.id.tresba);
        cuatrob = (RadioButton)findViewById(R.id.cuatroba);

        amenor=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,conmen);
        amenor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ametrosmen=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,conmetrosmen);
        ametrosmen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        smenor.setOnItemSelectedListener(this);
        smayor.setOnItemSelectedListener(this);
        smetrosmen.setOnItemSelectedListener(this);
        smetrosmay.setOnItemSelectedListener(this);

        smenor.setAdapter(amenor);
        smetrosmen.setAdapter(ametrosmen);


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == smenor.getId()) {
            conmay.clear();
            for (int i = smenor.getSelectedItemPosition(); i < conmen.length; i++) {
                conmay.add(conmen[i]);
            }
            amayor = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, conmay);
            amayor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            smayor.setAdapter(amayor);
            if (smenor.getSelectedItemPosition() == 0) {
                smayor.setEnabled(false);
                preciomen="";
            }
            else {
                smayor.setEnabled(true);
                preciomen=" and Precio>="+rangprecio[smenor.getSelectedItemPosition()];
            }
        }

        if(parent.getId() == smayor.getId()){
            if(conmay.size()==conmen.length)
                preciomay="";
            else
                preciomay=" and Precio<="+rangprecio[smenor.getSelectedItemPosition()+smayor.getSelectedItemPosition()];
        }

        if (parent.getId() == smetrosmen.getId()) {
            conmetrosmay.clear();
            for (int i = smetrosmen.getSelectedItemPosition(); i < conmetrosmen.length; i++) {
                conmetrosmay.add(conmetrosmen[i]);
            }
            ametrosmay = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, conmetrosmay);
            ametrosmay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            smetrosmay.setAdapter(ametrosmay);
            if (smetrosmen.getSelectedItemPosition() == 0) {
                smetrosmay.setEnabled(false);
                dimmen="";
            }
            else {
                smetrosmay.setEnabled(true);
                dimmen=" and Dimenciones>="+rangodim[smetrosmen.getSelectedItemPosition()];
            }
        }

        if(parent.getId() == smetrosmay.getId()){
            if(conmetrosmay.size()==conmetrosmen.length)
                dimmay="";
            else
                dimmay=" and Dimenciones<="+rangodim[smetrosmen.getSelectedItemPosition()+smetrosmay.getSelectedItemPosition()];
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void buscar(View v){
        if(todoh.isChecked())
            no_hab="";
        if(unoh.isChecked())
            no_hab=" and No_habitaciones=1";
        if(dosh.isChecked())
            no_hab=" and No_habitaciones=2";
        if(tresh.isChecked())
            no_hab=" and No_habitaciones=3";
        if(cuatroh.isChecked())
            no_hab=" and No_habitaciones>=4";

        if(todob.isChecked())
            no_ban="";
        if(unob.isChecked())
            no_ban=" and No_banos=1";
        if(dosb.isChecked())
            no_ban=" and No_banos=2";
        if(tresb.isChecked())
            no_ban=" and No_banos=3";
        if(cuatrob.isChecked())
            no_ban=" and No_banos>=4";
        Intent Renta = new Intent(FiltroRenta.this, ListaRenta.class);
        startActivity(Renta);
        finish();
    }
    @Override
    public void onBackPressed(){
        Intent Renta = new Intent(FiltroRenta.this, ListaRenta.class);
        startActivity(Renta);
        finish();
    }
}
