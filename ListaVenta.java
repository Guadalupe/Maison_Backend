package mx.ita.saic4_000.maison;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class ListaVenta extends AppCompatActivity {

    private ListView listview;
    ArrayList direccion = new ArrayList();
    ArrayList colonia = new ArrayList();
    ArrayList precio = new ArrayList();
    ArrayList imagen = new ArrayList();
    ArrayList ID = new ArrayList();
    ArrayList hab = new ArrayList();
    ArrayList ban = new ArrayList();
    ArrayList dim = new ArrayList();
    ImageView venta,renta,fil;
    TextView tventa,trenta,tfiltro;
    public static String id, col;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista);
        listview = (ListView) findViewById(R.id.listView);

        venta = (ImageView) findViewById(R.id.imageventa);
        venta.setImageResource(R.drawable.ic_sale_pressed);
        tventa = (TextView) findViewById(R.id.textventa);
        tventa.setTextColor(tventa.getContext().getResources().getColor(R.color.colorPrimaryDark));

        renta = (ImageView) findViewById(R.id.imagerenta);
        trenta = (TextView) findViewById(R.id.textrenta);
        fil = (ImageView) findViewById(R.id.imagefiltro);
        tfiltro = (TextView) findViewById(R.id.textfiltro);
        if(f==1) {
            venta.setImageResource(R.drawable.ic_sale_unpressed);
            tventa.setTextColor(Color.BLACK);
            fil.setImageResource(R.drawable.ic_filter_list_pressed);
            tfiltro.setTextColor(tventa.getContext().getResources().getColor(R.color.colorPrimaryDark));
            renta.setImageResource(R.drawable.ic_rent_unpressed);
            trenta.setTextColor(Color.BLACK);
        }
        descargarimagen();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {

                Intent Venta = new Intent(ListaVenta.this, Home_view.class);
                id = ID.get(position).toString();

                col = colonia.get(position).toString();

                startActivity(Venta);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //OBJETOS DEL MENU
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int icon = item.getItemId();

        //noinspection SimplifiableIfStatement
        //ACCION DE CADA ICONO
        switch (icon) {
            case R.id.Settings:
                Toast.makeText(ListaVenta.this, "Error 404.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Buscar:
                Toast.makeText(ListaVenta.this, "Error 404.", Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void descargarimagen() {
        direccion.clear();
        colonia.clear();
        precio.clear();
        imagen.clear();
        ID.clear();
        hab.clear();
        ban.clear();
        dim.clear();

        final ProgressDialog progresdialog = new ProgressDialog(ListaVenta.this);
        progresdialog.setMessage("Cargando...");
        progresdialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://s657539154.onlinehome.mx/lista_venta.php?cat="+Filtro.categoria+"&precmen="+ Filtro.preciomen+"&precmay="+ Filtro.preciomay+"&dimmen="+ Filtro.dimmen+"&dimmay="+ Filtro.dimmay+"&hab="+ Filtro.no_hab+"&ban="+ Filtro.no_ban, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    progresdialog.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            direccion.add(jsonArray.getJSONObject(i).getString("Direccion"));
                            colonia.add(jsonArray.getJSONObject(i).getString("Colonia"));
                            precio.add(jsonArray.getJSONObject(i).getString("Precio"));
                            imagen.add(jsonArray.getJSONObject(i).getString("Imagen"));
                            ID.add(jsonArray.getJSONObject(i).getString("ID_casa"));
                            hab.add(jsonArray.getJSONObject(i).getString("No_habitaciones"));
                            ban.add(jsonArray.getJSONObject(i).getString("No_banos"));
                            dim.add(jsonArray.getJSONObject(i).getString("Dimenciones"));
                        }
                        listview.setAdapter(new ImageAdapter(getApplicationContext()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }


    private class ImageAdapter extends BaseAdapter {
        Context ctx;
        LayoutInflater layoutinflater;
        SmartImageView smartimageview;
        TextView tvdireccion, tvcolonia, tvprecio, nohab, noban, dimencion;

        public ImageAdapter(Context applicationContext) {
            this.ctx = applicationContext;
            layoutinflater = (LayoutInflater) ctx.getSystemService(LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {

            return imagen.size();
        }

        @Override
        public Object getItem(int i) {

            return i;
        }

        @Override
        public long getItemId(int i) {

            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewGroup viewgroup = (ViewGroup) layoutinflater.inflate(R.layout.lista_formato, null);

            smartimageview = (SmartImageView) viewgroup.findViewById(R.id.imagen1);
            tvprecio = (TextView) viewgroup.findViewById(R.id.tvPrecio);
            tvdireccion = (TextView) viewgroup.findViewById(R.id.tvDireccion);
            tvcolonia = (TextView) viewgroup.findViewById(R.id.tvColonia);
            nohab = (TextView) viewgroup.findViewById(R.id.habitacion);
            noban = (TextView) viewgroup.findViewById(R.id.banos);
            dimencion = (TextView) viewgroup.findViewById(R.id.dim);

            String url = "http://s657539154.onlinehome.mx/imagenes/" + imagen.get(i).toString();
            Rect rect = new Rect(smartimageview.getLeft(), smartimageview.getTop(), smartimageview.getBottom(), smartimageview.getRight());

            smartimageview.setImageUrl(url, rect);
            DecimalFormat dec=new DecimalFormat("###,###.00");
            int prec = Integer.parseInt(precio.get(i).toString());
            tvprecio.setText(dec.format(prec).toString());
            tvdireccion.setText("Calle " + direccion.get(i).toString());
            tvcolonia.setText("Colonia " + colonia.get(i).toString());
            nohab.setText(hab.get(i).toString() + " hab.");
            noban.setText(ban.get(i).toString() + " bñ.");
            dimencion.setText(dim.get(i).toString() + " m²");

            return viewgroup;
        }
    }
    public void venta (View v){
        Filtro.preciomen="";
        Filtro.preciomay="";
        Filtro.dimmen="";
        Filtro.dimmay="";
        Filtro.no_hab="";
        Filtro.no_ban="";
        Filtro.categoria="Venta";
        venta.setImageResource(R.drawable.ic_sale_pressed);
        tventa.setTextColor(tventa.getContext().getResources().getColor(R.color.colorPrimaryDark));
        fil.setImageResource(R.drawable.ic_filter_list);
        tfiltro.setTextColor(Color.BLACK);
        renta.setImageResource(R.drawable.ic_rent_unpressed);
        trenta.setTextColor(Color.BLACK);
        descargarimagen();
    }
    public void renta (View v){
        Filtro.preciomen="";
        Filtro.preciomay="";
        Filtro.dimmen="";
        Filtro.dimmay="";
        Filtro.no_hab="";
        Filtro.no_ban="";
        Filtro.categoria="Renta";
        venta.setImageResource(R.drawable.ic_sale_unpressed);
        tventa.setTextColor(Color.BLACK);
        fil.setImageResource(R.drawable.ic_filter_list);
        tfiltro.setTextColor(Color.BLACK);
        renta.setImageResource(R.drawable.ic_rent_pressed);
        trenta.setTextColor(trenta.getContext().getResources().getColor(R.color.colorPrimaryDark));
        descargarimagen();
    }
    public static int f=0;
    public void filter (View v){
        Intent filtro = new Intent(ListaVenta.this,Filtro.class);
        startActivity(filtro);
        finish();
    }

}

