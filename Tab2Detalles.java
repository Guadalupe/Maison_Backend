package mx.ita.saic4_000.maison;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

/**
 * Created by hp on 03/11/2016.
 */
public class Tab2Detalles extends Fragment {
    String id;





    View rootView;
    TextView tvdireccion, tvcolonia, tvprecio,tvcategoria,tvbano,tvhabitacion,tvdimencion,tvnombre,tvcorreo,tvtelefono1,tvtelefono2;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tab2detalles, null);
        tvdireccion=(TextView)rootView.findViewById(R.id.Direccion);
        tvcolonia=(TextView)rootView.findViewById(R.id.Fracc);
        tvprecio=(TextView)rootView.findViewById(R.id.precio);
        tvcategoria=(TextView)rootView.findViewById(R.id.Categoria_rv);
        tvdimencion=(TextView)rootView.findViewById(R.id.superficie);
        tvbano=(TextView)rootView.findViewById(R.id.baño);
        tvhabitacion=(TextView)rootView.findViewById(R.id.room);
        tvnombre=(TextView)rootView.findViewById(R.id.Propi_inmovi);
        tvcorreo=(TextView)rootView.findViewById(R.id.email);
        tvtelefono1=(TextView)rootView.findViewById(R.id.tel1);
        tvtelefono2=(TextView)rootView.findViewById(R.id.tel2);
        if(ListaVenta.id!=null) {
            new Mostrardatos().execute("http://s657539154.onlinehome.mx/Detalles.php?id=" + ListaVenta.id);
        }
        if(ListaRenta.id!=null) {
            new Mostrardatos().execute("http://s657539154.onlinehome.mx/Detalles.php?id=" + ListaRenta.id);
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    private class Mostrardatos extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            JSONArray jsonArray=null;
            try {
                jsonArray=new JSONArray(result);
                DecimalFormat dec=new DecimalFormat("###,###.00");
                int prec = Integer.parseInt(jsonArray.getJSONObject(0).getString("Precio"));
                tvdireccion.setText("Calle "+jsonArray.getJSONObject(0).getString("Direccion"));
                tvcolonia.setText("Colonia "+jsonArray.getJSONObject(0).getString("Colonia"));
                tvprecio.setText(dec.format(prec).toString());
                tvcategoria.setText(jsonArray.getJSONObject(0).getString("Categoria"));
                tvhabitacion.setText(jsonArray.getJSONObject(0).getString("No_habitaciones")+" hab");
                tvbano.setText(jsonArray.getJSONObject(0).getString("No_banos")+" bñ");
                tvdimencion.setText(jsonArray.getJSONObject(0).getString("Dimenciones")+" m²");
                tvnombre.setText(jsonArray.getJSONObject(0).getString("Nombre"));
                tvcorreo.setText(jsonArray.getJSONObject(0).getString("Correo"));
                tvtelefono1.setText(jsonArray.getJSONObject(0).getString("Telefono1"));
                tvtelefono2.setText(jsonArray.getJSONObject(0).getString("Telefono2"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("respuesta", "The response is: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = readIt(is, len);
            return contentAsString;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }
}
