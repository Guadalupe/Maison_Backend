package mx.ita.saic4_000.maison;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by hp on 13/11/2016.
 */

//METODOS DE FRAGMENT
public class ViewPagerAdapter1 extends FragmentPagerAdapter{

    String[] tabtitlearrat = {"Habitacion", "Detalles", "Servcios"};



    public ViewPagerAdapter1(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Tab1Habitacion();
            case 1:
                return new Tab2Detalles();
            case 2:
                return new Tab3Servicios();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    //TITULOS DE TABS
    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
