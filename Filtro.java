package mx.ita.saic4_000.maison;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by saic4_000 on 27/04/2017.
 */
public class Filtro extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner smenor,smayor,smetrosmen,smetrosmay;
    ArrayAdapter<String> amenor,amayor,ametrosmen,ametrosmay;
    String[] conmen = new String[]{"Todos","$200,000.00","$300,000.00","$400,000.00","$500,000.00"
            ,"$600,000.00","$700,000.00","$800,000.00","$900,000.00","$1,000,000.00","$1,500,000.00","$2,000,000.00","$2,500,000.00"};
    String[] rangprecio = new String[]{"","200000","300000","400000","500000"
            ,"600000","700000","800000","900000","1000000","1500000","2000000","2500000"};
    String[] conmetrosmen = new String[]{"Todos","20 m\u00B2","25 m²","30 m²","35 m²"
            ,"40 m²","50 m²","55 m²","60 m²","65 m²"};
    String[] rangodim = new String[]{"","20","25","30","35"
            ,"40","50","55","60","65"};
    ArrayList<String> conmay = new ArrayList();
    ArrayList<String> conmetrosmay = new ArrayList();
    RadioButton todoh,unoh,dosh,tresh,cuatroh,todob,unob,dosb,tresb,cuatrob;
    RadioGroup hab,cat,ban;
    public static String preciomen="", preciomay="", dimmen="", dimmay="",no_hab="",no_ban="", categoria="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filtro);
        smenor=(Spinner)findViewById(R.id.spinnermenor);
        smayor=(Spinner)findViewById(R.id.spinnermayor);
        smetrosmen=(Spinner)findViewById(R.id.metrosmen);
        smetrosmay=(Spinner)findViewById(R.id.metrosmay);
        hab = (RadioGroup) findViewById(R.id.hab);
        cat = (RadioGroup) findViewById(R.id.categoria);
        ban = (RadioGroup) findViewById(R.id.ban);

        amenor=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,conmen);
        amenor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ametrosmen=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,conmetrosmen);
        ametrosmen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        smenor.setOnItemSelectedListener(this);
        smayor.setOnItemSelectedListener(this);
        smetrosmen.setOnItemSelectedListener(this);
        smetrosmay.setOnItemSelectedListener(this);

        smenor.setAdapter(amenor);
        smetrosmen.setAdapter(ametrosmen);
        categoria="Venta";
        no_ban="";
        no_hab="";
        hab.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.todohab:
                        no_hab="";
                        break;
                    case R.id.unohab:
                        no_hab=" and No_habitaciones=1";
                        break;
                    case R.id.doshab:
                        no_hab=" and No_habitaciones=2";
                        break;
                    case R.id.treshab:
                        no_hab=" and No_habitaciones=3";
                        break;
                    case R.id.cuatrohab:
                        no_hab=" and No_habitaciones>=4";
                        break;
                }
            }
        });

        cat.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){
                    case R.id.sale:
                        categoria="Venta";
                        conmen = new String[]{"Todos","$200,000.00","$300,000.00","$400,000.00","$500,000.00"
                                ,"$600,000.00","$700,000.00","$800,000.00","$900,000.00","$1,000,000.00","$1,500,000.00","$2,000,000.00","$2,500,000.00"};
                        rangprecio = new String[]{"","200000","300000","400000","500000"
                                ,"600000","700000","800000","900000","1000000","1500000","2000000","2500000"};

                        llenarsppinermen(conmen);
                        break;
                    case R.id.rent:
                        categoria="Renta";
                        conmen = new String[]{"Todos","$500.00","$600.00","$700.00","$800.00"
                                ,"$900.00","$1,000.00","$1,500.00","$2,000.00","$2,500.00"};
                        rangprecio = new String[]{"","500","600","700","800"
                                ,"900","1000","1500","2000","2500"};

                        llenarsppinermen(conmen);
                        break;
                }
            }

        });

        ban.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.todoba:
                        no_ban="";
                        break;
                    case R.id.unoba:
                        no_ban=" and No_banos=1";
                        break;
                    case R.id.dosba:
                        no_ban=" and No_banos=2";
                        break;
                    case R.id.tresba:
                        no_ban=" and No_banos=3";
                        break;
                    case R.id.cuatroba:
                        no_ban=" and No_banos>=4";
                        break;
                }

            }
        });
    }

    public void llenarsppinermen(String[] men){
        amenor=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,men);
        amenor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        smenor.setAdapter(amenor);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == smenor.getId()) {
            conmay.clear();
            for (int i = smenor.getSelectedItemPosition(); i < conmen.length; i++) {
                conmay.add(conmen[i]);
            }
            amayor = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, conmay);
            amayor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            smayor.setAdapter(amayor);
            if (smenor.getSelectedItemPosition() == 0) {
                smayor.setEnabled(false);
                preciomen="";

            }
            else {
                smayor.setEnabled(true);
                preciomen=" and Precio>="+rangprecio[smenor.getSelectedItemPosition()];
            }
        }

        if(parent.getId() == smayor.getId()){
            if(conmay.size()==conmen.length)
                preciomay = "";
            else
                preciomay=" and Precio<="+rangprecio[smenor.getSelectedItemPosition()+smayor.getSelectedItemPosition()];
        }

        if (parent.getId() == smetrosmen.getId()) {
            conmetrosmay.clear();
            for (int i = smetrosmen.getSelectedItemPosition(); i < conmetrosmen.length; i++) {
                conmetrosmay.add(conmetrosmen[i]);
            }
            ametrosmay = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, conmetrosmay);
            ametrosmay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            smetrosmay.setAdapter(ametrosmay);
            if (smetrosmen.getSelectedItemPosition() == 0) {
                smetrosmay.setEnabled(false);
                dimmen="";
            }
            else {
                smetrosmay.setEnabled(true);
                dimmen=" and Dimenciones>="+rangodim[smetrosmen.getSelectedItemPosition()];
            }
        }

        if(parent.getId() == smetrosmay.getId()){
            if(conmetrosmay.size()==conmetrosmen.length)
                dimmay="";
            else
                dimmay=" and Dimenciones<="+rangodim[smetrosmen.getSelectedItemPosition()+smetrosmay.getSelectedItemPosition()];
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public void buscar(View v){
        Intent filtro = new Intent(Filtro.this,ListaVenta.class);
        ListaVenta.f=1;
        startActivity(filtro);
        finish();
    }

    @Override
    public void onBackPressed(){
        Intent venta = new Intent(Filtro.this,ListaVenta.class);
        ListaVenta.f=0;
        startActivity(venta);
        finish();
    }
}
