package mx.ita.saic4_000.maison;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.github.snowdream.android.widget.SmartImageView;

/**
 * Created by saic4_000 on 24/05/2017.
 */
public class GalleryAdapter extends BaseAdapter{
    private Context context;
    private String[] images;
    private Rect[] rec;
    int background;

    public GalleryAdapter(Context c, String[] images, Rect[] rec) {
        context = c;
        this.images = images;
        this.rec = rec;
    }

    // returns the number of images
    public int getCount() {
        return images.length;
    }

    // returns the ID of an item
    public Object getItem(int position) {
        return position;
    }

    // returns the ID of an item
    public long getItemId(int position) {
        return position;
    }

    // returns an ImageView view
    public View getView(int position, View convertView, ViewGroup parent) {

        // create a ImageView programmatically
        SmartImageView imageView = new SmartImageView(context);
        imageView.setImageUrl(images[position],rec[position]); // set image in ImageView
        imageView.setLayoutParams(new Gallery.LayoutParams(200, 200)); // set ImageView param
        return imageView;
    }
}
