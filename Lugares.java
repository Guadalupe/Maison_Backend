package mx.ita.saic4_000.maison;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by saic4_000 on 25/05/2017.
 */
public class Lugares extends AppCompatActivity {

    private ListView listview;
    ArrayList NombreEm = new ArrayList();
    ArrayList DireccionEm = new ArrayList();
    ArrayList ColoniaEm = new ArrayList();
    ArrayList GiroEm = new ArrayList();
    ArrayList ContactoEm = new ArrayList();
    ArrayList ClaveEs = new ArrayList();
    ArrayList NombreEs = new ArrayList();
    ArrayList DireccionEs = new ArrayList();
    ArrayList ColoniaEs = new ArrayList();
    ArrayList TurnosEs = new ArrayList();
    ArrayList NivelEs = new ArrayList();
    ArrayList TelefonoEs = new ArrayList();
    ArrayList CalidadEs = new ArrayList();
    ArrayList NombreH = new ArrayList();
    ArrayList DireccionH = new ArrayList();
    ArrayList ColoniaH = new ArrayList();
    ArrayList TipoH = new ArrayList();
    ArrayList EspecialidadH = new ArrayList();
    ArrayList TelefonoH = new ArrayList();
    ArrayList NombreP = new ArrayList();
    ArrayList DireccionP = new ArrayList();
    ArrayList ColoniaP = new ArrayList();
    ArrayList NombreT = new ArrayList();
    ArrayList DireccionT = new ArrayList();
    ArrayList ColoniaT = new ArrayList();
    ArrayList ContactoT = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_lugares);

        listview=(ListView)findViewById(R.id.listViewlugares);

        switch (Home_view.s){
            case 1:
                descargarescuela();
                break;
            case 2:
                descargarhospital();
                break;
            case 3:
                descargartienda();
                break;
            case 4:
                descargarempresa();
                break;
            case 5:
                descargarparque();
                break;
        }

    }

    //PARQUE
    private void descargarparque() {
        String col=null;
        NombreP.clear();
        DireccionP.clear();
        ColoniaP.clear();
        if(ListaVenta.col!=null){
            col=ListaVenta.col;
        }
        if(ListaRenta.col!=null){
            col=ListaRenta.col;
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://s657539154.onlinehome.mx/Parque.php?colonia="+col, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            NombreP.add(jsonArray.getJSONObject(i).getString("Nombre"));
                            DireccionP.add(jsonArray.getJSONObject(i).getString("Direccion"));
                            ColoniaP.add(jsonArray.getJSONObject(i).getString("Colonia"));

                        }
                        listview.setAdapter(new ImageAdapterParque(getApplicationContext()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    private class ImageAdapterParque extends BaseAdapter {
        Context ctx;
        LayoutInflater layoutinflater;
        TextView nombre,direccion,colonia;
        public ImageAdapterParque(Context applicationContext) {
            this.ctx = applicationContext;
            layoutinflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return DireccionP.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewGroup viewgroup =(ViewGroup)layoutinflater.inflate(R.layout.parque, null);

            nombre = (TextView)viewgroup.findViewById(R.id.NombreP);
            direccion = (TextView)viewgroup.findViewById(R.id.DireccionP);
            colonia = (TextView)viewgroup.findViewById(R.id.ColoniaP);

            nombre.setText("Parque "+NombreP.get(i).toString());
            direccion.setText("Calle "+DireccionP.get(i).toString());
            colonia.setText("Colonia "+ColoniaP.get(i).toString());

            return viewgroup;
        }
    }

    //
    private void descargarhospital() {
        String col=null;
        NombreH.clear();
        DireccionH.clear();
        ColoniaH.clear();
        TipoH.clear();
        EspecialidadH.clear();
        TelefonoH.clear();
        if(ListaVenta.col!=null){
            col=ListaVenta.col;

        }
        if(ListaRenta.col!=null){
            col=ListaRenta.col;

        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://s657539154.onlinehome.mx/hospital.php?colonia="+col, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            NombreH.add(jsonArray.getJSONObject(i).getString("Nombre"));
                            DireccionH.add(jsonArray.getJSONObject(i).getString("Direccion"));
                            ColoniaH.add(jsonArray.getJSONObject(i).getString("Colonia"));
                            TipoH.add(jsonArray.getJSONObject(i).getString("Tipo"));
                            EspecialidadH.add(jsonArray.getJSONObject(i).getString("Especialidad"));
                            TelefonoH.add(jsonArray.getJSONObject(i).getString("Telefono"));
                        }
                        listview.setAdapter(new ImageAdapterHospital(getApplicationContext()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    private class ImageAdapterHospital extends BaseAdapter{
        Context ctx;
        LayoutInflater layoutinflater;
        TextView nombre,direccion,colonia,tipo,especialidad,telefono;
        public ImageAdapterHospital(Context applicationContext) {
            this.ctx = applicationContext;
            layoutinflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return DireccionH.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewGroup viewgroup =(ViewGroup)layoutinflater.inflate(R.layout.hospital, null);

            nombre = (TextView)viewgroup.findViewById(R.id.NombreH);
            direccion = (TextView)viewgroup.findViewById(R.id.DireccionH);
            colonia = (TextView)viewgroup.findViewById(R.id.ColoniaH);
            tipo = (TextView)viewgroup.findViewById(R.id.TipoH);
            especialidad = (TextView)viewgroup.findViewById(R.id.EspecialidadH);
            telefono = (TextView)viewgroup.findViewById(R.id.TelefonoH);

            nombre.setText("Hospital "+NombreH.get(i).toString());
            direccion.setText("Calle "+DireccionH.get(i).toString());
            colonia.setText("Colonia "+ColoniaH.get(i).toString());
            tipo.setText("Sector "+TipoH.get(i).toString());
            especialidad.setText("Tipo de servicio: "+EspecialidadH.get(i).toString());
            telefono.setText("Número de teléfono: "+TelefonoH.get(i).toString());

            return viewgroup;
        }
    }

    //EMPRESA
    private void descargarempresa() {
        String col=null;
        NombreEm.clear();
        DireccionEm.clear();
        ColoniaEm.clear();
        ContactoEm.clear();
        GiroEm.clear();
        if(ListaVenta.col!=null){
            col=ListaVenta.col;

        }
        if(ListaRenta.col!=null){
            col=ListaRenta.col;

        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://s657539154.onlinehome.mx/empresa.php?colonia="+col, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            NombreEm.add(jsonArray.getJSONObject(i).getString("Nombre"));
                            DireccionEm.add(jsonArray.getJSONObject(i).getString("Direccion"));
                            ColoniaEm.add(jsonArray.getJSONObject(i).getString("Colonia"));
                            GiroEm.add(jsonArray.getJSONObject(i).getString("Giro"));
                            ContactoEm.add(jsonArray.getJSONObject(i).getString("Contacto"));
                        }
                        listview.setAdapter(new ImageAdapterEmpresa(getApplicationContext()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    private class ImageAdapterEmpresa extends BaseAdapter{
        Context ctx;
        LayoutInflater layoutinflater;
        TextView nombre,direccion,colonia,giro,contacto;
        public ImageAdapterEmpresa(Context applicationContext) {
            this.ctx = applicationContext;
            layoutinflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return DireccionEm.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewGroup viewgroup =(ViewGroup)layoutinflater.inflate(R.layout.empresa, null);

            nombre = (TextView)viewgroup.findViewById(R.id.NombreEm);
            direccion = (TextView)viewgroup.findViewById(R.id.DireccionEm);
            colonia = (TextView)viewgroup.findViewById(R.id.ColoniaEm);
            giro = (TextView)viewgroup.findViewById(R.id.GiroEm);
            contacto = (TextView)viewgroup.findViewById(R.id.ContactoEm);

            nombre.setText("Empresa "+NombreEm.get(i).toString());
            direccion.setText("Calle "+DireccionEm.get(i).toString());
            colonia.setText("Colonia "+ColoniaEm.get(i).toString());
            giro.setText("Giro de la empresa: "+GiroEm.get(i).toString());
            contacto.setText("Número de teléfono"+ContactoEm.get(i).toString());

            return viewgroup;
        }
    }

    //ESCUELA
    private void descargarescuela() {
        String col=null;
        ClaveEs.clear();
        NombreEs.clear();
        DireccionEs.clear();
        ColoniaEs.clear();
        TurnosEs.clear();
        NivelEs.clear();
        TelefonoH.clear();
        CalidadEs.clear();
        if(ListaVenta.col!=null){
            col=ListaVenta.col;

        }
        if(ListaRenta.col!=null){
            col=ListaRenta.col;

        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://s657539154.onlinehome.mx/escuela.php?colonia="+col, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            ClaveEs.add(jsonArray.getJSONObject(i).getString("Clave_esc"));
                            NombreEs.add(jsonArray.getJSONObject(i).getString("Nombre"));
                            DireccionEs.add(jsonArray.getJSONObject(i).getString("Direccion"));
                            ColoniaEs.add(jsonArray.getJSONObject(i).getString("Colonia"));
                            TurnosEs.add(jsonArray.getJSONObject(i).getString("Turnos"));
                            NivelEs.add(jsonArray.getJSONObject(i).getString("Nivel"));
                            TelefonoEs.add(jsonArray.getJSONObject(i).getString("Telefono"));
                            CalidadEs.add(jsonArray.getJSONObject(i).getString("Calidad"));
                        }
                        listview.setAdapter(new ImageAdapterEscuela(getApplicationContext()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private class ImageAdapterEscuela extends BaseAdapter{
        Context ctx;
        LayoutInflater layoutinflater;
        TextView clave,nombre,direccion,colonia,turnos,nivel,telefono,calidad;
        public ImageAdapterEscuela(Context applicationContext) {
            this.ctx = applicationContext;
            layoutinflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return DireccionEs.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewGroup viewgroup =(ViewGroup)layoutinflater.inflate(R.layout.escuela, null);

            clave = (TextView)viewgroup.findViewById(R.id.Clavees);
            nombre = (TextView)viewgroup.findViewById(R.id.NombreEs);
            direccion = (TextView)viewgroup.findViewById(R.id.DireccionEs);
            colonia = (TextView)viewgroup.findViewById(R.id.ColoniaEs);
            turnos = (TextView)viewgroup.findViewById(R.id.TurnoEs);
            calidad = (TextView)viewgroup.findViewById(R.id.CalidadEs);
            telefono = (TextView)viewgroup.findViewById(R.id.TelefonoEs);
            nivel = (TextView)viewgroup.findViewById(R.id.NivelEs);

            nombre.setText("Escuela "+NombreEs.get(i).toString());
            direccion.setText("Calle "+DireccionEs.get(i).toString());
            colonia.setText("Colonia "+ColoniaEs.get(i).toString());
            clave.setText("Clave: "+ClaveEs.get(i).toString());
            turnos.setText("Turno(s): "+TurnosEs.get(i).toString());
            telefono.setText("Número de teléfono: "+TelefonoEs.get(i).toString());
            nivel.setText("Nivel: "+NivelEs.get(i).toString());
            calidad.setText("Calidad "+CalidadEs.get(i).toString()+"%");

            return viewgroup;
        }
    }

    //TIENDA
    private void descargartienda() {
        String col=null;
        NombreT.clear();
        DireccionT.clear();
        ColoniaT.clear();
        ContactoT.clear();
        if(ListaVenta.col!=null){
            col=ListaVenta.col;

        }
        if(ListaRenta.col!=null){
            col=ListaRenta.col;

        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://s657539154.onlinehome.mx/tienda.php?colonia="+col, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            NombreT.add(jsonArray.getJSONObject(i).getString("Nombre"));
                            DireccionT.add(jsonArray.getJSONObject(i).getString("Direccion"));
                            ColoniaT.add(jsonArray.getJSONObject(i).getString("Colonia"));
                            ContactoT.add(jsonArray.getJSONObject(i).getString("Contacto"));

                        }
                        listview.setAdapter(new ImageAdapterTienda(getApplicationContext()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    private class ImageAdapterTienda extends BaseAdapter{
        Context ctx;
        LayoutInflater layoutinflater;
        TextView nombre,direccion,colonia,contacto;
        public ImageAdapterTienda(Context applicationContext) {
            this.ctx = applicationContext;
            layoutinflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return DireccionT.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewGroup viewgroup =(ViewGroup)layoutinflater.inflate(R.layout.tienda, null);

            nombre = (TextView)viewgroup.findViewById(R.id.NombreT);
            direccion = (TextView)viewgroup.findViewById(R.id.DireccionT);
            colonia = (TextView)viewgroup.findViewById(R.id.ColoniaT);
            contacto = (TextView)viewgroup.findViewById(R.id.ContactoT);

            nombre.setText("Sucursal "+NombreT.get(i).toString());
            direccion.setText("Calle "+DireccionT.get(i).toString());
            colonia.setText("Colonia "+ColoniaT.get(i).toString());
            contacto.setText("Número de teléfono: "+ContactoT.get(i).toString());

            return viewgroup;
        }
    }

}
