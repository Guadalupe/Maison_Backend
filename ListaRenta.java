package mx.ita.saic4_000.maison;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class ListaRenta extends AppCompatActivity {

    private ListView listview;
    ArrayList direccion = new ArrayList();
    ArrayList colonia = new ArrayList();
    ArrayList precio = new ArrayList();
    ArrayList imagen = new ArrayList();
    ArrayList ID = new ArrayList();
    ArrayList hab = new ArrayList();
    ArrayList ban = new ArrayList();
    ArrayList dim = new ArrayList();
    ImageView renta;
    TextView trenta;
    public static String id, col;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista);
        listview = (ListView) findViewById(R.id.listView);
        descargarimagen();

        renta = (ImageView) findViewById(R.id.imagerenta);
        renta.setImageResource(R.drawable.ic_rent_pressed);
        trenta = (TextView) findViewById(R.id.textrenta);
        trenta.setTextColor(trenta.getContext().getResources().getColor(R.color.colorPrimaryDark));

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent Renta = new Intent(ListaRenta.this, Home_view.class);
                ListaVenta.col=null;
                ListaVenta.id=null;
                id=ID.get(i).toString();
                col=colonia.get(i).toString();
                startActivity(Renta);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    //OBJETOS DEL MENU
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int icon = item.getItemId();

        //noinspection SimplifiableIfStatement
        //ACCION DE CADA ICONO
        switch (icon){
            case R.id.Settings:
                Toast.makeText(ListaRenta.this, "Error 404.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Buscar:
                Toast.makeText(ListaRenta.this, "Error 404.",Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void descargarimagen() {
        direccion.clear();
        colonia.clear();
        precio.clear();
        imagen.clear();
        ID.clear();
        hab.clear();
        ban.clear();
        dim.clear();

        final ProgressDialog progresdialog = new ProgressDialog(ListaRenta.this);
        progresdialog.setMessage("Cargando...");
        progresdialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://s657539154.onlinehome.mx/lista_renta.php?precmen="+FiltroRenta.preciomen+"&precmay="+FiltroRenta.preciomay+"&dimmen="+FiltroRenta.dimmen+"&dimmay="+FiltroRenta.dimmay+"&hab="+FiltroRenta.no_hab+"&ban="+FiltroRenta.no_ban, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    progresdialog.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            direccion.add(jsonArray.getJSONObject(i).getString("Direccion"));
                            colonia.add(jsonArray.getJSONObject(i).getString("Colonia"));
                            precio.add(jsonArray.getJSONObject(i).getString("Precio"));
                            imagen.add(jsonArray.getJSONObject(i).getString("Imagen"));
                            ID.add(jsonArray.getJSONObject(i).getString("ID_casa"));
                            hab.add(jsonArray.getJSONObject(i).getString("No_habitaciones"));
                            ban.add(jsonArray.getJSONObject(i).getString("No_banos"));
                            dim.add(jsonArray.getJSONObject(i).getString("Dimenciones"));
                        }
                        listview.setAdapter(new ImageAdapter(getApplicationContext()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }



    private class ImageAdapter extends BaseAdapter {
        Context ctx;
        LayoutInflater layoutinflater;
        SmartImageView smartimageview;
        TextView tvdireccion1, tvcolonia2, tvprecio3,nohab,noban,dimencion;

        public ImageAdapter(Context applicationContext) {
            this.ctx = applicationContext;
            layoutinflater = (LayoutInflater) ctx.getSystemService(LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {

            return imagen.size();
        }

        @Override
        public Object getItem(int i) {

            return i;
        }

        @Override
        public long getItemId(int i) {

            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewGroup viewgroup = (ViewGroup) layoutinflater.inflate(R.layout.lista_formato, null);

            smartimageview = (SmartImageView) viewgroup.findViewById(R.id.imagen1);
            tvdireccion1 = (TextView) viewgroup.findViewById(R.id.tvDireccion);
            tvcolonia2 = (TextView) viewgroup.findViewById(R.id.tvColonia);
            tvprecio3 = (TextView) viewgroup.findViewById(R.id.tvPrecio);
            nohab = (TextView) viewgroup.findViewById(R.id.habitacion);
            noban = (TextView) viewgroup.findViewById(R.id.banos);
            dimencion = (TextView) viewgroup.findViewById(R.id.dim);

            String url = "http://s657539154.onlinehome.mx/imagenes/" + imagen.get(i).toString();
            Rect rect = new Rect(smartimageview.getLeft(), smartimageview.getTop(), smartimageview.getBottom(), smartimageview.getRight());

            smartimageview.setImageUrl(url, rect);

            DecimalFormat dec=new DecimalFormat("###,###.00");
            int prec = Integer.parseInt(precio.get(i).toString());
            tvprecio3.setText(dec.format(prec).toString());
            tvdireccion1.setText("Calle " + direccion.get(i).toString());
            tvcolonia2.setText("Colonia " + colonia.get(i).toString());
            nohab.setText(hab.get(i).toString()+" hab.");
            noban.setText(ban.get(i).toString()+" bñ.");
            dimencion.setText(dim.get(i).toString()+" m²");

            return viewgroup;
        }
    }

    public void venta (View v){
        Filtro.preciomen="";
        Filtro.preciomay="";
        Filtro.dimmen="";
        Filtro.dimmay="";
        Filtro.no_hab="";
        Filtro.no_ban="";
        Intent Venta = new Intent(ListaRenta.this, ListaVenta.class);
        startActivity(Venta);
    }
    public void renta (View v){


    }
}